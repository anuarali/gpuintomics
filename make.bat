@echo off 

set CXX=g++
::-Wall -Wextra 
set CXXFLAGS=-std=c++11 -g
set CXXLDFLAGS=-D CL_TARGET_OPENCL_VERSION=100
set CXX_CL_FLAGS=-isystem %OCL_ROOT%/include/ -L%OCL_ROOT%/lib/x86_64/ -lOpenCL


set SRC=
for /R %%f in (*.cpp) do call set SRC=%%SRC%% %%f

echo %SRC%
:: setlocal enableDelayedExpansion
::  -I./OpenCL/include -L./OpenCL/lib -lOpenCL -lOpenCL

:: %CXX_CL_FLAGS%  %CXXLDFLAGS%
@echo on 
%CXX% %CXXFLAGS% %SRC% %CXX_CL_FLAGS% -o main %CXXLDFLAGS%