#pragma once
//----------------------------------------------------------------------------------------
/**
 * \file       RMath.h
 * \author     Alikhan Anuarbekov
 * \date       2022/11/14
 * \brief      Implementation of the common function from the base R toolkit
 *
 *  This file contains the implementation of the required functions from the R language.
 *  Theoretically - original source code of the R language is already written in C++ and 
 *  can be more effective, but the majority of the following code is used on CPU before the actual
 *  Run() on GPU/CPU and won't be counted, so the effectiveness of this functions isn't critical 
 * 
*/
//----------------------------------------------------------------------------------------
#include <cstdint>
#include <stdint.h>
#include <vector>

#define _USE_MATH_DEFINES
#include <cmath>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

namespace anuarali
{
    /// Class that holds implementation of the base R functions
    /**
    This class contains all the necessary functions from the R base toolkit to run
    the algorithm of IntOMICS. Most of the functions can be imported from other
    C++ libraries such as OpenGL Mathematics (GLM).
    */
    class RMath
    {
    public:
        /// Calculate logarithm of the Gamma function
        /**
         *  The gamma function is basically factorial for the non-integer numbers.
         *  Gamma function is already implemented in <cmath> and GPU libraries,
         *  but since R language has a wrapper of log gamma (x), we will
         *  immitate the same function for the people, who want to compare original
         *  R implementation vs C++ implementation 
         * 
         *  \param[in] value input value of the log gamma (x)
         *  \return log gamma(value)
         * 
        */ 
        static double lgamma(double value);

        /// Calculate mean of each column and return it as matrix (1 x nCol), e.g., vector
        /**
         *  This function calculates mean of each column and return it as matrix (1 x nCol), e.g., vector
         *  ! Note : Allocates new matrix !
         * 
         *  \param[in] matrix pure input matrix, 2D array
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *  \return (1 x nCol) matrix, each column contains mean of the 
        */ 
        static double** colMeans(double** matrix, uint32_t nRow, uint32_t nCol);

        /// Calculate covariance of matrix columns
        /**
         *  This function calculates covariance of the columns of input matrix
         *  ! Note : Allocates new matrix !
         *  ! Note: internally allocates 3 matrices !
         *  
         *  \param[in] matrix pure input matrix, 2D array
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *  \return (nCol x nCol) matrix, each cell (i,j) = covariance of column i and column j
        */ 
        static double** cov(double** matrix, uint32_t nRow, uint32_t nCol);

        /// Allocate and set to zero new vector
        /**
         *  This function allocates new vector(1D array) of given size and set every element to 0
         *  ! Note : Allocates new array !
         *  
         *  \param[in] n number of elements of vector -> 1D array
         *  \return (nCol) vector, all elements are zeroed
        */ 
        static double* zeros(uint32_t n);
        /// Allocate and set to zero new matrix
        /**
         *  This function allocates new matrix(2D array) of given size and set every element to 0
         *  ! Note : Allocates new matrix !
         *  
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *  \return (nRow x nCol) matrix, each cell is set to zero
        */ 
        static double** zeros(uint32_t row, uint32_t col);

        /// Allocate and set to zero new boolean matrix
        /**
         *  This function allocates new boolean matrix(2D array) of given size and set every element to 0=False
         *  ! Note : Allocates new matrix !
         *  
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *  \return (nRow x nCol) matrix, each cell is set to zero=False
        */ 
        static bool** zeros_bool(uint32_t row, uint32_t col);

        

        /// Allocate a diagonal matrix with a given value
        /**
         *  This function allocates new matrix(2D array) of given size and set every element to 0
         *  Then, we set diagonal elements to the given value.
         *  ! Note : Allocates new matrix !
         *  
         *  \param[in] value value at the diagonal
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *  \return (nRow x nCol) matrix, diagonal is set to value, others to zero
        */ 
        static double** diag(double value, uint32_t row, uint32_t col);
        
        /// Allocate a matrix that is a transposed version of input matrix
        /**
         *  This function allocates new matrix(2D array) of the transposed size as input matrix. 
         *  Then, it fills its values so that the allocated matrix is a transposed version of input matrix.
         *  
         *  ! Note : Allocates new matrix !
         * 
         *  \param[in] mat input matrix
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *  \return (nRow x nCol) matrix, is equal to transposed input matrix
        */ 
        static double** matrixTranspose(double** mat, uint32_t row, uint32_t col);

        /// Allocate a matrix that contain sum of two input matrices
        /**
         *  This function allocates new matrix(2D array) of the same size as input matrices. 
         *  Then, it fills its values so that the allocated matrix is a sum of 2 input matrices
         *  
         *  ! Note : Allocates new matrix !
         * 
         *  \param[in] first input matrix 1
         *  \param[in] second input matrix 2
         *  \param[in] nRow number of rows of matrices -> 1st dimension
         *  \param[in] nCol number of columns of matrices -> 2nd dimension
         *  \return (nRow x nCol) matrix, is equal to sum of first + second
        */ 
        static double** matrixSum(double** first, double** second, uint32_t row, uint32_t col);

        /// Allocate a matrix that contain multiplication of two matrices
        /**
         *  This function allocates new matrix(2D array) of the size as matrix multiplication result:
         *  
         *       (row1,   col1)   x     (row2,     col2)    =    (row1,     col2)
         * 
         *  Then, it fills its values so that the allocated matrix is a multiplication of 2 input matrices
         *  
         *  ! Note : Allocates new matrix !
         * 
         *  \param[in] first input matrix 1
         *  \param[in] second input matrix 2
         *  \param[in] row1 number of rows of matrix 1 -> 1st dimension
         *  \param[in] col1 number of columns of matrix 1 -> 2nd dimension
         *  \param[in] row2 number of rows of matrix 2 -> 1st dimension
         *  \param[in] col2 number of columns of matrix 2 -> 2nd dimension
         *  \return (row1 x col2) matrix, is equal to matrix multiplication of first * second
        */ 
        static double** matrixMult(double** first, double** second, uint32_t row1, uint32_t col1, uint32_t row2, uint32_t col2);

        /// Multiplies input matrix by a constant. Modifies original matrix
        /**
         *  This function perform the multiplication of every element of matrix by value.
         *  Since matrix is a 2D array, it will go over first dim and multiply every cell. 
         *  
         *  ! Note : Modifies input matrix !
         * 
         *  \param[in] matrix input matrix
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *
        */ 
        static void matrixMultConst(double value, double** mat, uint32_t row, uint32_t col);

        /// Divides input matrix by a constant. Modifies original matrix
        /**
         *  This function perform the division of every element of matrix by value.
         *  Since matrix is a 2D array, it will go over first dim and multiply every cell. 
         *  
         *  ! Note : Modifies input matrix !
         * 
         *  \param[in] matrix input matrix
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *
        */ 
        static void matrixDivConst(double value, double** mat, uint32_t row, uint32_t col);

        /// Adds a constant to the input matrix. Modifies original matrix
        /**
         *  This function perform the addition of constant to every element of matrix.
         *  Constant is given by parameter value.. 
         *  
         *  ! Note : Modifies input matrix !
         * 
         *  \param[in] value constant to be added
         *  \param[in] matrix input matrix
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *
        */ 
        static void matrixAddConst(double value, double** mat, uint32_t row, uint32_t col);


        /// Frees all the memory of a given matrix
        /**
         *  This function deallocates the matrix that was allocated by RMath functions. 
         *  Since matrix is a 2D array, it will go over first dim and free every row. 
         *  Then, it will free entire pointer. Pointer is undefined after this call.
         *  
         *  ! Note : Modifies input matrix !
         * 
         *  \param[in] matrix input matrix
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *
        */ 
        static void deleteMatrix(double** matrix, uint32_t row, uint32_t col);

        /// Frees all the memory of a given matrix
        /**
         *  This function deallocates the matrix that was allocated by RMath functions. 
         *  Since matrix is a 2D array, it will go over first dim and free every row. 
         *  Then, it will free entire pointer. Pointer is undefined after this call.
         *  
         *  ! Note : Modifies input matrix !
         * 
         *  \param[in] matrix input matrix
         *  \param[in] nRow number of rows of matrix -> 1st dimension
         *  \param[in] nCol number of columns of matrix -> 2nd dimension
         *
        */ 
        static void deleteMatrix(bool** matrix, uint32_t row, uint32_t col);


        static uint32_t* sample_int(uint32_t n, uint32_t size, bool replace = false);


        static void set_seed(uint32_t seed);

        /**
         * Just calls multiple single runif, number of calls is specified by
         * <num>
        */
        static double* runif(uint32_t num, double min=0.0, double max=1.0);

        static double runif(double min=0.0, double max=1.0);

        /**
         * 
         * Implementation is taken from:
         * https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform
        */
        static double rnorm(double mean=0.0, double SD=1.0);

        /**
         * Just calls multiple single rnorm, number of calls is specified by
         * <num>
        */
        static double* rnorm(uint32_t num, double mean=0.0, double SD=1.0);



        struct LM
        {
            double slope = 0.0;
            double intercept = 0.0;
            double slope_p_value = 0.0;
        };

        /**
         * Computes the linear regression model of the equivalent:
         *     X = 1,         ..., N                   // iteration number
         *     Y = values[0], ..., values[N-1]         // values of such iteration
        */
        static LM lm_p_value(double* values, uint32_t num_values);

        /**
         * Just calls lm_p_value with vector converted to array
        */
        static LM lm_p_value(std::vector<double> values);


        /**
         * Compute Student's T-distribution's CDF -> e.g. inverse to PDF
        */
        static double pt(double value, uint32_t df);



        /**
         *  Compute (n k) combinatorial sum
        */
        static unsigned long k_from_n(uint32_t n, uint32_t k);
        
    protected:
        static uint32_t* sample_int_noreplace(uint32_t n, uint32_t size);
    };

    
}