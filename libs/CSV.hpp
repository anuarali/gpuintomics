#pragma once

#include <iostream>
#include <cstdlib>
#include <stdlib.h>
#include <any>
#include <fstream>
#include <string>
#include <cstdio>
#include <cassert>

namespace anuarali
{
    
    class CSV
    {
    public:
        enum ECSVElemType
        {
            CSV_DOUBLE, CSV_STRING
        };

        union UCSVElem
        {
            char* str = nullptr;
            double d;
        };

        struct CSVElem
        {
            UCSVElem value {};
            ECSVElemType type;
            uint32_t num_chars = 0;
        };

        struct CSVData
        {
            CSVElem** data = nullptr;
            uint32_t ROW = 0;
            uint32_t COL = 0;

            ~CSVData()
            {
                if(data == nullptr) return;
                for(uint32_t i = 0; i < ROW; i++)
                {
                    for(uint32_t j = 0; j < COL; j++)
                    {
                        if(data[i][j].type == CSV_STRING)
                        {
                            delete[] data[i][j].value.str;
                        }
                    }
                    delete[] data[i];
                }
                delete[] data;
            }
        };


        enum LastEscapeChar
        {
            COMMA,
            NEWLINE,
            EndEOF,
            NONE
        };

        inline static LastEscapeChar readWord(FILE* f, CSVElem* elem)
        {
            LastEscapeChar ret = NONE;
            char c;
            std::vector<char> word;
            bool str_ended = false;
            elem->type = CSV::CSV_DOUBLE;

            while(true)
            {
                c = fgetc(f);
                if( (c == ' ') && 
                    (elem->type != CSV_STRING))
                {
                    continue;
                }


                switch(c)
                {
                    case '\n':
                    {
                        ret = NEWLINE;
                        break;
                    }
                    case ' ':
                    {
                        if((elem->type == CSV_STRING) && (!str_ended))
                        {
                            word.push_back(c);
                        }
                        break;
                    }
                    case '\"':
                    {
                        if(elem->type == CSV::CSV_STRING)
                        {
                            str_ended = true;
                        }else{
                            elem->type = CSV::CSV_STRING;
                        } 
                        
                        break;
                    }
                    case EOF:
                    {
                        ret = EndEOF;
                        break;
                    }
                    case ',':
                    {
                        ret = COMMA;
                        break;
                    }
                    default:
                    {                       
                        word.push_back(c);
                        break;
                    }
                }

                if(ret == NONE) continue;



                if(elem->type == CSV_STRING)
                {
                    elem->value.str = new char[word.size()+1];
                    for(uint32_t i = 0; i < word.size(); i++)
                    {
                        elem->value.str[i] = word[i];
                    }
                    elem->value.str[word.size()] = '\0';
                }else{
                    std::string w_str = std::string(word.begin(), word.end());
                    if(w_str == std::string("")) // last line = empty string
                    {
                        elem->type = CSV_STRING;
                        elem->value.str = nullptr;
                    }
                    else{
                        double value = std::atof(w_str.c_str());
                        elem->value.d = value;
                    }   
                }
                break;      
            }
            return ret;
        }

        inline static CSVData readCSVFile(std::string filepath)
        {
            FILE* f = fopen(filepath.c_str(), "r");

            CSVData ret;

            
            std::vector<CSVElem> elems;
            uint32_t currRow = 0;
            uint32_t nCols = 0;

            std::vector<CSVElem*> finalData;

            while(true)
            {
                CSVElem elem;
                LastEscapeChar last = CSV::readWord(f, &elem);
                elems.push_back(elem);

                
                if(last == NEWLINE || last == EndEOF)
                {
                    if(nCols == 0) 
                    {
                        nCols = elems.size();   
                    }
                    
                    if((elems.size() == 1) && (elem.type == CSV_STRING) && (elem.value.str == nullptr))
                    {
                        break;
                    }
                    
                    assert(elems.size() == nCols);

                    CSVElem* dataRow = new CSVElem[elems.size()];

                    for(uint32_t i = 0; i < elems.size(); i++)
                    {
                        dataRow[i] = elems[i];
                    }

                    finalData.push_back(dataRow);

                    elems.clear();
                    currRow++;
                }

                if(last == EndEOF) break;
            }
            fclose(f);


            ret.COL = nCols;
            ret.ROW = currRow;
            ret.data = new CSVElem*[currRow];
            for(uint32_t i = 0; i < currRow; i++)
            {
                ret.data[i] = finalData[i];
            }
            finalData.clear();
            
            for(uint32_t i = 0; i < 1; i++)
            {
                for(uint32_t j = 0; j < ret.COL; j++)
                {
                    if(ret.data[i][j].type == CSV_DOUBLE)
                    {
                        std::cout << "d[" << ret.data[i][j].value.d << "] ";
                    }
                    else{
                        std::cout << "[" << ret.data[i][j].value.str << "]";
                    }  
                }
                printf("\n");
            }

            printf("Total rows: %d\n", ret.ROW - 1);

            return ret;
        }
    };
}