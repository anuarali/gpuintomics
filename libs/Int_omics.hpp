#pragma once

#include <inttypes.h>
#include <vector>
#include <string>
#include <cassert>
#include <map>
#include <utility>
#include <algorithm>

#include "RMath.hpp"
#include "CSV.hpp"

namespace anuarali
{
    /**
     * Global settings class -----> is it really needed?
    */
    class GlobalSettings
    {
    public:
        GlobalSettings(std::string inputData, std::string pkData) {
            this->readInputData(inputData);
            this->readPKTable(pkData);
            this->computeUBPseudoHistogram();
            this->UB(0.0);
            this->UB(0.1);
            this->UB(0.2);
        }


        double UB(double beta) 
        {
            double ret = 0.0;

            // use super formula from my paper work
            /**
             *  UB = sum_Ni sum_Npi  
             *               C_ni_npi * 
             *               (
             *                  (Ni-Npi)log(2) + sum_k=0^Npi (Npi k) exp(-beta * ((Npi - k)))
             *               )
            */
            for(auto [key, value] : this->inputData.C_ni_npi)
            {
                uint32_t Ni = key.first;
                uint32_t Npi = key.second;
                uint32_t C_ni_npi = value;
                double half_Ni_minus_Npi = 0.5 * (Ni - Npi);

                double const_part = log(4.0) * (half_Ni_minus_Npi);

                double iterative_part = 0.0;
                for(uint32_t k = 0; k <= Npi; k++)
                {
                    double inside = (Npi - k) *  GlobalSettings::BELIEF_PK_IN_GE + k * (1 - GlobalSettings::BELIEF_PK_IN_GE) + half_Ni_minus_Npi;
                    iterative_part += exp(-beta * inside) * RMath::k_from_n(Npi, k);
                }
                iterative_part = log(iterative_part);

                ret += C_ni_npi * (const_part + iterative_part);
            }

            printf("\nBeta: %f ---> UB: %f\n", beta, ret);

            return ret;
        }
    protected:
        inline static const double BELIEF_PK_IN_GE = 1.0;
        inline static const double BELIEF_UNKNOWN = 0.5;
        inline static const double BELIEF_PK_NOT_GE = 0.0;

        enum EdgeType
        {
            PRESENT,
            ABSENT
        };
        static EdgeType to_etype(std::string type)
        {
            if(type == std::string("absent"))
            {
                return EdgeType::ABSENT;
            }
            if(type == std::string("present"))
            {
                return EdgeType::PRESENT;
            }
            printf("Error, unknown value of prior interaction: \"%s\"", type.c_str());
            assert(false);
        }

        static double pk_value(EdgeType etype)
        {
            switch(etype)
            {
                case PRESENT:
                {
                    return BELIEF_PK_IN_GE;
                }
                case ABSENT:
                {
                    return BELIEF_PK_NOT_GE;
                }
                default:
                {
                    assert(false);
                }
            }
            assert(false);
            return -1.0;
        }
        

        struct PriorElement
        {
            uint32_t from_id = 0;
            uint32_t to_id = 0;

            EdgeType edge_type = EdgeType::ABSENT;

            void SetData(uint32_t from, uint32_t to, EdgeType type)
            {
                this->from_id = from;
                this->to_id = to;
                this->edge_type = type;
            }
        };

        typedef std::pair<uint32_t, uint32_t> pair_ni_npi;

        struct InputGeneData
        {
            double** expr_table = nullptr;
            double** B_prior_mat = nullptr;
            std::vector<PriorElement> prior_table;
            std::vector<std::string> gene_names;
            std::map<pair_ni_npi, uint32_t> C_ni_npi;


            uint32_t N_GENES = 0;
            uint32_t N_SAMPLES = 0;
        };

    protected:
        InputGeneData inputData {};


        void readInputData(std::string filepath)
        {
            CSV::CSVData cdata = CSV::readCSVFile(filepath);

            
            this->inputData.N_GENES = cdata.COL - 1;
            this->inputData.N_SAMPLES = cdata.ROW - 1;
            this->inputData.expr_table = RMath::zeros(inputData.N_SAMPLES, inputData.N_GENES);
            this->inputData.B_prior_mat = RMath::zeros(inputData.N_GENES, inputData.N_GENES);

            // initialize a matrix to a 0.5 value
            RMath::matrixAddConst(GlobalSettings::BELIEF_UNKNOWN, 
                                    this->inputData.B_prior_mat, 
                                    this->inputData.N_GENES, this->inputData.N_GENES);

            for(uint32_t i = 0; i < this->inputData.N_GENES; i++)
            {
                this->inputData.B_prior_mat[i][i] = GlobalSettings::BELIEF_PK_NOT_GE; // set to 0 every i->i
            }




            for(uint32_t i = 1; i < (cdata.ROW); i++)
            {
                for(uint32_t j = 1; j < (cdata.COL); j++)
                {
                    assert(cdata.data[i][j].type == CSV::CSV_DOUBLE);
                    this->inputData.expr_table[i-1][j-1] = cdata.data[i][j].value.d;
                }   
            }

            this->inputData.gene_names.reserve(cdata.COL-1);

            for(uint32_t i = 1; i < (cdata.COL); i++)
            {
                assert(cdata.data[0][i].type == CSV::CSV_STRING);
                this->inputData.gene_names.push_back(cdata.data[0][i].value.str);
            }
        }

        void readPKTable(std::string filepath)
        {
            CSV::CSVData cdata = CSV::readCSVFile(filepath);
            assert(cdata.COL == 7);

            for(uint32_t i = 1; i < cdata.ROW; i++)
            {
                assert(cdata.data[i][1].type == CSV::CSV_STRING);
                assert(cdata.data[i][2].type == CSV::CSV_STRING);
                assert(cdata.data[i][6].type == CSV::CSV_STRING);

                std::string from = std::string(cdata.data[i][1].value.str);
                auto itr_from = std::find(this->inputData.gene_names.begin(), this->inputData.gene_names.end(), from);
                if(itr_from == this->inputData.gene_names.end())
                {
                    printf("gene: %s was not found in gene names!\n", from.c_str());
                    assert(itr_from != this->inputData.gene_names.end());
                }
                
                std::string to = std::string(cdata.data[i][2].value.str);
                auto itr_to = std::find(this->inputData.gene_names.begin(), this->inputData.gene_names.end(), to);
                if(itr_to == this->inputData.gene_names.end())
                {
                    printf("gene: %s was not found in gene names!\n", to.c_str());
                    assert(itr_to != this->inputData.gene_names.end());
                }
                
                uint32_t idx_from = itr_from - this->inputData.gene_names.begin();
                uint32_t idx_to = itr_to - this->inputData.gene_names.begin();
                std::string stype = std::string(cdata.data[i][6].value.str);
                EdgeType etype = to_etype(stype);

                PriorElement pelem {};
                pelem.SetData(idx_from, idx_to, etype);

                this->inputData.prior_table.push_back(pelem);
                this->inputData.B_prior_mat[idx_from][idx_to] = pk_value(etype);
            }
        }

        void computeUBPseudoHistogram()
        {
            printf("Computing pseudo histogram!\n");
            for(uint32_t i = 0; i < this->inputData.N_GENES; i++)
            {
                // compute N_i
                uint32_t N_i = 0;
                // compute N_p_i
                uint32_t N_p_i = 0;

                for(uint32_t j = 0; j < this->inputData.N_GENES; j++)
                {
                    N_i += this->inputData.B_prior_mat[j][i] != GlobalSettings::BELIEF_PK_NOT_GE; // count all possible inputs
                    N_p_i += this->inputData.B_prior_mat[j][i] == GlobalSettings::BELIEF_PK_IN_GE; // count all prior inputs
                }

                pair_ni_npi hist_key = std::make_pair(N_i,N_p_i);

                if(this->inputData.C_ni_npi.count(hist_key) == 0)
                {
                    this->inputData.C_ni_npi[hist_key] = 0;
                }

                this->inputData.C_ni_npi[hist_key] += 1;
                printf("Gene: %s has Ni: %d, Npi: %d\n", this->inputData.gene_names[i].c_str(), N_i, N_p_i);
            }

            for(auto [key, value] : this->inputData.C_ni_npi)
            {
                printf("[Ni: %d, Npi: %d] -> C_ni_npi: %d\n", key.first, key.second, value);
            }
        }
    };
    /**
     * Beta class
    */
    class Beta
    {
    public:
        Beta(GlobalSettings* settings) : settings(settings) {}

        double getLogUB() { return this->logUB;}

        double getValue() { return this->value;}
        void setValue(double value)
        {
            this->value = value;
            this->logUB = log(settings->UB(value));
        }

        void setSD(double value) {this->SD = value;}
        double getSD() { return this->SD; }
        double updateSD(double acceptance)
        {
            if(acceptance > 0.44)
            {   
                this->SD = log(exp(this->SD) + 0.05);
            }
            else{
                this->SD = log(exp(this->SD) - 0.05);
            }
            return this->SD;
        }

        Beta* next()
        {
            Beta* next_beta = new Beta(this->settings);
            double new_value;
            new_value = RMath::rnorm(this->value, this->SD);
            if(new_value < 0.5)
            {
                new_value = 0.5;
            }

            next_beta->setValue(new_value);
            next_beta->setSD(this->SD);

            return next_beta;
        }

    protected:
        GlobalSettings* settings = nullptr;

        double value = 0.0;
        double SD = 5.0;
        double logUB = 0.0;
    };
    /**
     * Network class
    */
    class Network
    {
    public:
        inline static double computeAcceptance(Network* curr, Network* cand)
        {
            return 0.0;
        }


        Network(GlobalSettings* settings) : settings(settings) {}

        bool get(uint32_t from, uint32_t to)
        {
            uint32_t linear_id = this->N_GENES * from + to;

            //(c & (1 << i)) != 0;

            return encoded_data[linear_id / 8];
        }

        void set(uint32_t from, uint32_t to, bool val)
        {

        }

        Network* MC3(Beta* curr_beta)
        {
            return nullptr; // TODO
        }
    
    protected:
        GlobalSettings* settings = nullptr;

        unsigned char* encoded_data = nullptr;
        uint32_t N_GENES = 0;
    };

    

    

    /**
     * Main class
    */
    class IntOMICS
    {
    public:
        IntOMICS(std::string input, std::string pk) : settings(input, pk){}

        void set_MBR_prob(double value) { IntOMICS::MBR_PROB = value;}
        void set_seed1(unsigned long value) { IntOMICS::SEED1 = value;}
        void set_seed2(unsigned long value) { IntOMICS::SEED2 = value;}

        void run()
        {
            this->first_adapt_phase();
        }

    protected:
        inline static double MBR_PROB = 0.07;
        inline static unsigned long SEED1 = 12345;
        inline static unsigned long SEED2 = 54321;

    protected:
        GlobalSettings settings;
        
        std::vector<double> beta_values {};
        std::vector<bool> beta_acceptance {};
        std::vector<double> all_sd {};

        // ???? should keep this ???
        std::vector<Network> networks {};

        void sample_first_network()
        {
            //RMath::set_seed(IntOMICS::SEED1);
            // TODO -> sample first network
            // 1) either do the sample linear chain as IntOMICS
            // 2) or do the appropriate path in tripartite graph
        }

        void first_adapt_phase()
        {
            RMath::set_seed(IntOMICS::SEED1); // ! only one call to set.seed, not like in original IntOMICS
            this->sample_first_network();

            this->acceptance_loop(100, 100);
            this->acceptance_loop(100, 200);
            this->acceptance_loop(200, 400);
        }

        double compute_beta_acceptance(uint32_t last_check)
        {
            auto start = beta_acceptance.begin();
            uint32_t size = beta_acceptance.size();
            if(size > last_check)
            {
                start = beta_acceptance.end() - last_check;
                size = last_check;
            }
            auto count = std::count(
                start, 
                beta_acceptance.end(), 
                true);
            

            double acceptance = ((double)count) / ((double)size);
            return acceptance;
        }

        void acceptance_loop(uint32_t round_check, uint32_t last_check)
        {
            Beta* curr_beta;
            Network* curr_net;

            double acc_rate = 0.0;
            uint32_t iter = 0;

            while((acc_rate > 0.28) &&
                  (acc_rate < 0.66))
            {
                // sample new network
                Network* cand_net = curr_net->MC3(curr_beta);
                double acc_net = Network::computeAcceptance(curr_net, cand_net);
                double u = RMath::runif(0.0, 1.0);
                u = log(u);

                if(u < acc_net)
                {
                    curr_net = cand_net;
                }

                // sample new beta
                Beta* cand_beta = curr_beta->next();
                double acc_beta = cand_beta->getLogUB() - curr_beta->getLogUB(); // TODO add energy compute
                
                u = RMath::runif(0.0, 1.0);
                u = log(u);

                bool has_accepted_beta = false;
                if(u < acc_beta)
                {
                    curr_beta = cand_beta;
                    has_accepted_beta = true;
                }

                // Compute acceptance
                double beta_accept_rate = this->compute_beta_acceptance(last_check);

                // if every K-th iteration, perform update of variance
                iter++;
                if((iter % round_check) == 0)
                {
                    // update SD
                    double SD = curr_beta->updateSD(beta_accept_rate);
                    all_sd.push_back(SD);
                }

                // it should be pushed only after SD update !
                beta_acceptance.push_back(has_accepted_beta);
                beta_values.push_back(curr_beta->getValue());
            }

        }


    };

    



}