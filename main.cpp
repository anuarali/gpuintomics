//#include "libs/intomics_CPU.hpp"
#include "libs/Int_omics.hpp"
#include "libs/RMath.hpp"


void printMatrix(double** matrix, uint32_t row, uint32_t col, uint32_t offset_row, uint32_t offset_col)
{
    for(uint32_t i = offset_row; i < offset_row+row; i++)
    {
        for(uint32_t j = offset_col; j < offset_col+col; j++)
        {
            printf("%f ", matrix[i][j]);
        }
        printf("\n");
    } 
}


int main()
{
    //anuarali::IntOMICS_CPU* intomics = new anuarali::IntOMICS_CPU();
    //intomics->SetInputData("input_data.csv");
    //intomics->SetPriorTable("input_PK.csv");
    //intomics->Initialize();
    //intomics->Run(true);
    //delete intomics;
    
    // 1 circ, 3 miRNA, 3 mRNA 
    std::string input_filepath = "7_input_data.csv";
    std::string pk_filepath = "7_PK_data.csv";

    input_filepath = "input_data.csv";
    pk_filepath = "input_PK.csv";

    input_filepath = "input_data_1x4x100.csv";
    pk_filepath = "input_PK_1x4x100.csv";

    anuarali::IntOMICS* intomics = new anuarali::IntOMICS(input_filepath, pk_filepath);
    //intomics->run();    

    return 0;
}