#include "../libs/RMath.hpp"

#include <cmath>
#include <cstdlib>
#include <stdlib.h>
#include <stdexcept>

#include <limits>
#include <random>
#include <utility>


namespace anuarali
{
    double RMath::lgamma(double value)
    {      
        // Gamma function in math.h (!on top of that -> already implemented on GPU !)
        // see https://docs.nvidia.com/cuda/cuda-math-api/group__CUDA__MATH__DOUBLE.html
        return log(tgamma(value));
    }

    double** RMath::colMeans(double** matrix, uint32_t nRow, uint32_t nCol)
    {
        double** ret = RMath::zeros(1, nCol);

        for(uint32_t i = 0; i < nCol; i++)
        {
            for(uint32_t j = 0; j < nRow; j++)
            {
                ret[0][i] += matrix[j][i];
            }
            ret[0][i] = ret[0][i] / nRow;
        }
        return ret;
    }

    double** RMath::cov(double** matrix, uint32_t nRow, uint32_t nCol)
    {
        double** ret = RMath::zeros(nCol, nCol);

        double** tmpMatrix = RMath::zeros(nRow, nCol);
        double** col_means = RMath::colMeans(matrix, nRow, nCol); // 1, nCol

        // subtract colmean for every cell
        for(uint32_t i = 0; i < nRow; i++)
        {
            for(uint32_t j = 0; j < nCol; j++)
            {
                tmpMatrix[i][j] = matrix[i][j] - col_means[0][j];
            }
        }


        // perform cross product of columns
        for(uint32_t i = 0; i < nCol; i++)
        {
            for(uint32_t j = 0; j < nCol; j++)
            {
                double value = 0.0;

                for(uint32_t row_i = 0; row_i < nRow; row_i++)
                {
                    value += tmpMatrix[row_i][i] * tmpMatrix[row_i][j];
                }

                ret[i][j] = value;
            }
        }

        // normalize by (nCol - 1)
        RMath::matrixDivConst(nRow - 1, ret, nCol, nCol);

        RMath::deleteMatrix(col_means, 1, nCol);
        RMath::deleteMatrix(tmpMatrix, nRow, nCol);

        return ret;
    }

    double* RMath::zeros(uint32_t n)
    {
        double* ret = new double[n];
        for(uint32_t i = 0; i < n; i++)
        {
            ret[i] = 0.0;
        } 
        return ret;
    }

    bool** RMath::zeros_bool(uint32_t row, uint32_t col)
    {
        bool** ret = new bool*[row];
        for(uint32_t i = 0; i < row; i++)
        {
            ret[i] = new bool[col];

            for(uint32_t j = 0; j < col; j++)
            {
                ret[i][j] = false;
            }
        } 
        return ret;
    }

    double** RMath::zeros(uint32_t row, uint32_t col)
    {
        double** ret = new double*[row];
        for(uint32_t i = 0; i < row; i++)
        {
            ret[i] = zeros(col);
        } 
        return ret;
    }

    double** RMath::diag(double value, uint32_t row, uint32_t col)
    {
        double** ret = zeros(row, col);

        uint32_t min_of_two = std::min(row, col);

        for(uint32_t i = 0; i < min_of_two; i++)
        {
            ret[i][i] = value;
        }  

        return ret;
    }

    double** RMath::matrixSum(double** first, double** second, uint32_t row, uint32_t col)
    {
        double** ret = RMath::zeros(row, col);

        for(uint32_t i = 0; i < row; i++)
        {
            for(uint32_t j = 0; j < col; j++)
            {
                ret[i][j] = first[i][j] + second[i][j];
            }
        }

        return ret;
    }

    void RMath::deleteMatrix(double** matrix, uint32_t row, uint32_t col)
    {
        for(uint32_t i = 0; i < row; i++)
        {
            delete[] matrix[i];
        }

        delete[] matrix;
    }

    void RMath::deleteMatrix(bool** matrix, uint32_t row, uint32_t col)
    {
        for(uint32_t i = 0; i < row; i++)
        {
            delete[] matrix[i];
        }

        delete[] matrix;
    }

    double** RMath::matrixMult(double** first, double** second, uint32_t row1, uint32_t col1, uint32_t row2, uint32_t col2)
    {
        double** ret = RMath::zeros(row1, col2);

        for(uint32_t ret_i = 0; ret_i < row1; ret_i++)
        {
            for(uint32_t ret_j = 0; ret_j < col2; ret_j++)
            {
                double value = 0.0;
                for(uint32_t i = 0; i < col1; i++)
                {
                    for(uint32_t j = 0; j < row2; j++)
                    {
                        value += first[ret_i][i] * second[j][ret_j];
                    }
                }

                ret[ret_i][ret_j] = value;
            }
        }

        return ret;
    }

    void RMath::matrixAddConst(double value, double** mat, uint32_t row, uint32_t col)
    {
        for(uint32_t i = 0; i < row; i++)
        {
            for(uint32_t j = 0; j < col; j++)
            {
                mat[i][j] += value;
            }
        }
    }


    double** RMath::matrixTranspose(double** mat, uint32_t row, uint32_t col)
    {
        double** ret = RMath::zeros(col, row);

        for(uint32_t i = 0; i < row; i++)
        {
            for(uint32_t j = 0; j < col; j++)
            {
                ret[j][i] = mat[i][j];
            }
        }

        return ret;
    }

    void RMath::matrixMultConst(double value, double** mat, uint32_t row, uint32_t col)
    {
        for(uint32_t i = 0; i < row; i++)
        {
            for(uint32_t j = 0; j < col; j++)
            {
                mat[i][j] = mat[i][j] * value;
            }
        }
    }
    void RMath::matrixDivConst(double value, double** mat, uint32_t row, uint32_t col)
    {
        for(uint32_t i = 0; i < row; i++)
        {
            for(uint32_t j = 0; j < col; j++)
            {
                mat[i][j] = mat[i][j] / value;
            }
        }
    }
    
    uint32_t* RMath::sample_int(uint32_t n, uint32_t size, bool replace)
    {
        if(replace == false)
            return RMath::sample_int_noreplace(n, size);

        throw std::runtime_error("sample.int(..., replace = TRUE) not implemented yet!");
    }

    uint32_t* RMath::sample_int_noreplace(uint32_t n, uint32_t size)
    {
        uint32_t* sample_arr = new uint32_t[n];
        uint32_t* ret = new uint32_t[size];

        for(uint32_t i = 0; i < n; i++)
        {
            ret[i] = (n - i - 1);
        }

        uint32_t K = (size == n) ? (size - 1) : size; // if we sample everything, we don't need to sample at iteration
        /**
         * This part is a bit tricky:
         * 
         *  at first we start with: 
         *   5 4 3 2 1 0
         * 
         *  then, at each iteration we will sample an index 
         *   and move it to the right, swapping with left most value of right part:
         * 
         *  3 | 4 5 2 1 0
         *      ^ 
         *      this is the left most value of right part
         *  ^
         *  this is already sampled part
         *  
         *  this will make us then sample from n-1 elements, left part is ready
         *  
         *  3 1 | 5 2 4 0
         *  .....
         *  
        */
        for(uint32_t i = 0; i < (K); i++)
        {
            uint32_t sampled_idx = (uint32_t)(rand() % (size - i)); // sample uniformly for (n),(n-1),...(2)

            // perform a swap
            uint32_t tmp = sample_arr[n - sampled_idx - 1];   // revert an index, because we sample from right part
            sample_arr[n - sampled_idx - 1] = sample_arr[i];   
            sample_arr[i] = tmp;

            ret[i] = tmp;  // also save to the return array -> because it can have different size than our indices
        }

        if(size == n)// if we sample everything, we don't need to sample at iteration
        {
            ret[n-1] = sample_arr[n-1]; // just take the last element
        }
        

        delete[] sample_arr;

        return ret;
    }


    void RMath::set_seed(uint32_t seed)
    {
        srand(seed);
    }

    double* RMath::runif(uint32_t num, double min, double max)
    {
        double* ret = new double[num];

        for(uint32_t i = 0; i < num; i++)
        {
            ret[i] = RMath::runif(min, max);
        }
        
        return ret;
    }

    double RMath::runif(double min, double max)
    {
        double f;
        f = (double)rand() / RAND_MAX;
        f = min + f * (max - min);
        return f;
    }

    double RMath::rnorm(double mean, double SD)
    {
        constexpr double epsilon = std::numeric_limits<double>::epsilon();
        constexpr double two_pi = 2.0 * M_PI;

        //create two random numbers, make sure u1 is greater than epsilon
        double u1, u2;
        do
        {
            u1 = RMath::runif();
        }
        while (u1 <= epsilon);
        u2 = RMath::runif();

        //compute z0 and z1
        auto mag = SD * sqrt(-2.0 * log(u1));
        auto z0  = mag * cos(two_pi * u2) + mean;
        auto z1  = mag * sin(two_pi * u2) + mean;

        // only modification -> choose 50/50 from two generated Gaussian numbers
        u1 = RMath::runif();
        double ret = (u1 < 0.5) ? z0 : z1;

        return ret;
    }

    double* RMath::rnorm(uint32_t num, double mean, double SD)
    {
        double* ret = new double[num];
        for(uint32_t i = 0; i < num; i++)
        {
            ret[i] = RMath::rnorm(mean, SD);
        }
        
        return ret;
    }

    RMath::LM RMath::lm_p_value(double* values, uint32_t num_values)
    {
        /** First ----> calculate slope + intercept coefficients
         * 
         *  e.g. --->    Y ~ X * slope + intercept 
         *               Y = values 
         *               X = iterations = 1, ..., num_values
         *  Formula is from:
         *  https://towardsdatascience.com/linear-regression-by-hand-ee7fe5a751bf
        */
        
        double x_sum = (num_values * (num_values+1)) / 2.0;  // 1 + ... + N   =   N * (N+1) / 2
        double x_sq_sum = x_sum * (2.0 * num_values + 1.0) / 3.0;  // 1^2 +... + N^2   =  N * (N+1) * (2N+1) / 6

        double y_sum = 0.0;
        double xy_sum = 0.0;
        for(uint32_t i = 0; i < num_values; i++)
        {
            xy_sum += (i+1) * values[i];
            y_sum += values[i];
        }

        // Slope
        double slope = (num_values * xy_sum) - (x_sum * y_sum);
        slope = slope / (num_values * x_sq_sum - x_sum * x_sum);
        // Intercept
        double intercept = (y_sum - slope * x_sum);
        intercept = intercept / num_values;

        printf("slope: %f, intercept: %f\n", slope, intercept);
        
        /**
         * Second -> calculate t value by formula:
         * 
         *  t = slope / SD(slope)
         * 
         *                 RSE ^ 2
         *  SD(slope)^2 =    -----------------------------
         *                 sqrt [ sum(x – mean(x))^2        ]
         * 
         * https://cw.fel.cvut.cz/b211/_media/courses/b4m36san/san_regression.pdf
        */
        double upper = 0;
        double lower = 0;
        double y_mean = y_sum / num_values;

        for(uint32_t i = 0; i < num_values; i++)
        {
            double tmp = (double)(i+1) - (double)(num_values+1) / 2.0;
            lower += tmp * tmp;
            
            tmp = values[i] - (i+1) * slope - intercept;
            upper += tmp * tmp;
        }
        upper = upper / (num_values - 2);

        printf("SD(slope):%f\n", sqrt(upper / lower));

        double t_value = slope / sqrt(upper / lower);
        printf("t-value: %f\n", t_value);

        double p_value = 2.0 * RMath::pt(-abs(t_value), num_values-2);
        printf("p-value: %f\n", p_value);

        RMath::LM ret;
        ret.intercept = intercept;
        ret.slope = slope;
        ret.slope_p_value = p_value;

        return ret;
    }
    RMath::LM RMath::lm_p_value(std::vector<double> values)
    {
        return RMath::lm_p_value(&values[0], values.size());
    }

    



    /**
     * Taken from: 
     * https://stackoverflow.com/questions/10927859/incomplete-beta-function-in-raw-c
     * https://codeplea.com/incomplete-beta-function-c
    */
    double incbeta(double a, double b, double x);

    double RMath::pt(double value, uint32_t df)
    {
         /*The cumulative distribution function (CDF) for Student's t distribution*/
        double x = (value + sqrt(value * value + 1.0 * df)) / (2.0 * sqrt(value * value + 1.0 * df));
        double prob = incbeta(df/2.0, df/2.0, x);
        return prob;
    }

    /**
     * Taken from: 
     * https://stackoverflow.com/questions/10927859/incomplete-beta-function-in-raw-c
     * https://codeplea.com/incomplete-beta-function-c
    */
    #define STOP 1.0e-8
    #define TINY 1.0e-30

    double incbeta(double a, double b, double x)
    {
        if (x < 0.0 || x > 1.0) return 1.0/0.0;

        /*The continued fraction converges nicely for x < (a+1)/(a+b+2)*/
        if (x > (a+1.0)/(a+b+2.0)) {
            return (1.0-incbeta(b,a,1.0-x)); /*Use the fact that beta is symmetrical.*/
        }

        /*Find the first part before the continued fraction.*/
        const double lbeta_ab = lgamma(a)+lgamma(b)-lgamma(a+b);
        const double front = exp(log(x)*a+log(1.0-x)*b-lbeta_ab) / a;

        /*Use Lentz's algorithm to evaluate the continued fraction.*/
        double f = 1.0, c = 1.0, d = 0.0;

        int i, m;
        for (i = 0; i <= 200; ++i) {
            m = i/2;

            double numerator;
            if (i == 0) {
                numerator = 1.0; /*First numerator is 1.0.*/
            } else if (i % 2 == 0) {
                numerator = (m*(b-m)*x)/((a+2.0*m-1.0)*(a+2.0*m)); /*Even term.*/
            } else {
                numerator = -((a+m)*(a+b+m)*x)/((a+2.0*m)*(a+2.0*m+1)); /*Odd term.*/
            }

            /*Do an iteration of Lentz's algorithm.*/
            d = 1.0 + numerator * d;
            if (fabs(d) < TINY) d = TINY;
            d = 1.0 / d;

            c = 1.0 + numerator / c;
            if (fabs(c) < TINY) c = TINY;

            const double cd = c*d;
            f *= cd;

            /*Check for stop.*/
            if (fabs(1.0-cd) < STOP) {
                return front * (f-1.0);
            }
        }

        return 1.0/0.0; /*Needed more loops, did not converge.*/
    }

    unsigned long RMath::k_from_n(uint32_t n, uint32_t k)
    {
        if (k == 0) return 1;

        /*
        Extra computation saving for large K,
        using property:
        N choose K = N choose (N-K)
        */
        if (k > n / 2) return RMath::k_from_n(n, n - k); 

        long res = 1; 

        for (int i = 1; i <= k; ++i)
        {
            res *= n - i + 1;
            res /= i;
        }

        return res;
    }

}