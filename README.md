# GPUIntOMICS

C++ implementation of GPU-parallelized version of the IntOMICS algorithm 

## Intro

The original IntOMICS algorithm was used to compute the bayesian network from the gene expression/CNV/methylation data. The main problems of the algorithm, as admitted by the authors of the original paper, were the memory and performance limitations. This implementation is trying to solve both problems.

Original IntOMICS paper: https://doi.org/10.1186/s12859-022-04891-9

Original IntOMICS source code: https://gitlab.ics.muni.cz/bias/intomics/-/tree/master/

The algorithm is converted from the R language to a low-level C/C++ and further improved by utilizing GPU parallelization

## Feature overview

*   [x] Preserved structure of the source files as the original source code in R
*   [x] GPU implementation written in OpenCL - doesn't require NVIDIA hardware
*   [x] Support on Windows systems
*   [ ] Support of the CNV/METH data inputs TODO
*   [ ] Support on linux/OSX systems
*   [ ] Rcpp package format for the usage with R language

## Contents

*   [Getting started](#getting-started)
    *   [Requirements](#requirements)
    *   [Install](#install)
*   [License](#license)

## Getting Started

So how do you get this template to work for your project? It is easier than you think.

### Requirements

* g++ installed on machine with the minimum of **STD=C++11** support
* OpenCL enabled on GPU driver: https://streamhpc.com/blog/2015-03-16/how-to-install-opencl-on-windows/

### Install

Use git bash to clone this repository into your computer.

```
git clone https://gitlab.com/kopino4-templates/readme-template
```

on Windows 10 systems just run *make.bat* from CMD to compile the source code:
```
make.bat
```

## License
[MIT](https://choosealicense.com/licenses/mit/)

